# Car Management Dashboard
Car Management Dashboard merupakan tampilan pada rental mobil yang terintegrasi dengan restfulAPI dan express js

## How to run
Pertama lakukan penginstalan menggunakan npm

```bash
npm install
```

lalu ubah file config yakni username, password, dan database yang akan digunakan serta ubah dialect menjadi postgres

```bash
nano config/config.json
```

lalu lakukan migration

```bash
npm migrate
```

Terakhir, anda dapat menjalankan aplikasinya

```bash
npm run start
```

## Endpoints

### Endpoints Routes
GET "/" => Routes pada list car

GET "/form" => Routes pada form tambah data

GET "/form/:id" => Routes pada edit data

### Endpoints REST API
GET /api/v1/cars -> Getting all cars

GET /api/v1/cars/:id -> Getting a specific car by id

POST /api/v1/cars -> Creating a car

PUT /api/v1/cars -> Update a car

DELETE /api/v1/cars/:id -> Deleting a specific car by id

## Directory Structure

```
.
├── config
│   └──config.json
├── controllers
│   ├── car_get_all.js
│   ├── car-by-id.js
│   ├── car-delete.js
│   ├── car-get.js
│   ├── car-post.js
│   ├── car-put.js
│   ├── car-upload.js
│   ├── form.js
│   └── index.js
├── migrations
│   ├── 20220422124611-create-sizes.js
│   └── 20220422124610-create-cars.js
├── models
│   ├── cars.js
│   ├── sizes.js
│   └── index.js
├── public
│   ├── css
│   ├── img
│   └── upload
├── views
│   ├── templates
│   │   ├── footer.ejs
│   │   └── header.ejs
│   ├── form.ejs
│   └── list.ejs
├── .gitignore
├── erd.jpg
├── index.js
├── package-lock.json
├── package.json
└── README.md
```

## ERD
![Entity Relationship Diagram](erd.jpg)

