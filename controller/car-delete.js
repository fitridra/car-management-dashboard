const { user } = require("pg/lib/defaults");
const { Cars } = require("../models");

async function carDelete(req, res) {
  let id = req.params.id;

  try {
    await Cars.destroy({where: {id}})
  
    return res.json({message: 'user deleted!'});
  } catch (error) {
    console.log(error)
    return res.status(500).json({error: 'something went wrong'})
  }
}

module.exports = carDelete;
